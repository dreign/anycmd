﻿
namespace Anycmd.ViewModel
{

    /// <summary>
    /// 模型视图控制器响应模型
    /// </summary>
    public class ResponseData : IViewModel
    {
        private string _resultType = "success";
        private bool _success = true;

        /// <summary>
        /// 
        /// </summary>
        public bool success
        {
            get { return _success; }
            set
            {
                _success = value;
                if (!_success && _resultType == "success")
                {
                    _resultType = "";
                }
            }
        }
        /// <summary>
        /// 取值：success、info、warning、danger
        /// </summary>
        public string resultType
        {
            get
            {
                return _resultType;
            }
            private set { _resultType = value; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string msg { get; set; }
        /// <summary>
        /// 一般只有在表格内编辑模式的时候才用赋此值
        /// 为处理表格内编辑模式（RowEditor）游离对象时返回Id
        /// </summary>
        public object id { get; set; }

        public ResponseData Info()
        {
            this.resultType = "info";
            return this;
        }

        public ResponseData Warning()
        {
            this.resultType = "warning";
            return this;
        }

        public ResponseData Danger()
        {
            this.resultType = "danger";
            return this;
        }

        public ResponseData Error()
        {
            this.resultType = "error";
            this.success = false;
            return this;
        }
    }
}
