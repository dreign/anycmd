﻿
namespace Anycmd.ViewModel
{
    using Anycmd.Host;
    using System;

    public class UIViewViewModel
    {
        public static readonly UIViewViewModel Empty = new UIViewViewModel(UIViewState.Empty, "无名页面");

        public UIViewViewModel(UIViewState page, string title)
        {
            this.UIView = page;
            this.Id = page.Id;
            this.Tooltip = page.Tooltip;
            this.Icon = page.Icon;
            this.Title = title;
        }

        public UIViewState UIView { get; private set; }

        public Guid Id { get; private set; }

        public string Title { get; private set; }

        public string Tooltip { get; private set; }

        public string Icon { get; private set; }
    }
}
