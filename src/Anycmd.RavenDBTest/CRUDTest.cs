﻿
namespace Anycmd.RavenDBTest
{
    using Host.AC.Infra;
    using Raven.Abstractions.Commands;
    using System;
    using Xunit;

    public class CRUDTest
    {
        [Fact]
        public void Crud()
        {
            var appSystem = new AppSystem
            {
                Id = Guid.NewGuid(),
                Name = "test",
                Code = "test",
                AllowDelete = 0,
                AllowEdit = 0,
                Description = "test",
                Icon = "icon1",
                ImageUrl = string.Empty,
                IsEnabled = 1,
                PrincipalID = Guid.NewGuid(),
                SortCode = 1,
                SSOAuthAddress = string.Empty
            };
            using (var session = CSStore.SingleInstance.OpenSession("anycmd"))
            {
                session.Store(appSystem);
                session.SaveChanges();
            }
            using (var session = CSStore.SingleInstance.OpenSession("anycmd"))
            {
                var entity = session.Load<AppSystem>(appSystem.Id);
                Assert.NotNull(entity);
                Assert.Equal(appSystem.PrincipalID, entity.PrincipalID);
            }
            using (var session = CSStore.SingleInstance.OpenSession("anycmd"))
            {
                var entity = session.Load<AppSystem>(appSystem.Id);
                entity.Name = "new name";
                session.SaveChanges();
            }
            using (var session = CSStore.SingleInstance.OpenSession("anycmd"))
            {
                var entity = session.Load<AppSystem>(appSystem.Id);
                Assert.Equal("new name", entity.Name);
            }
            using (var session = CSStore.SingleInstance.OpenSession("anycmd"))
            {
                session.Advanced.Defer(new DeleteCommandData { Key = "AppSystems/" + appSystem.Id.ToString() });
                session.SaveChanges();
                var entity = session.Load<AppSystem>(appSystem.Id);
                Assert.Null(entity);
            }
        }
    }
}
