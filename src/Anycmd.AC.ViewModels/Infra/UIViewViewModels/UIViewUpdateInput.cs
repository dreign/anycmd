﻿
namespace Anycmd.AC.Infra.ViewModels.UIViewViewModels
{
    using Host.AC.ValueObjects;
    using Model;
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewUpdateInput : IInputModel, IUIViewUpdateInput
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Tooltip { get; set; }
    }
}
