﻿
namespace Anycmd.AC.Infra.ViewModels.UIViewViewModels
{
    using Host.AC.ValueObjects;
    using Model;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewCreateInput : EntityCreateInput, IInputModel, IUIViewCreateInput
    {
        /// <summary>
        /// 
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Tooltip { get; set; }
    }
}
