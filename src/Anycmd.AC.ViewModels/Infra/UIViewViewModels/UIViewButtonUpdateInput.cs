﻿using System;

namespace Anycmd.AC.Infra.ViewModels.UIViewViewModels
{
    using Host.AC.ValueObjects;
    using Model;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewButtonUpdateInput : IInputModel, IUIViewButtonUpdateInput
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid? FunctionID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int IsEnabled { get; set; }
    }
}
