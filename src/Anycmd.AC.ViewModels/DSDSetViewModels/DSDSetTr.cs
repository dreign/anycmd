﻿
namespace Anycmd.AC.ViewModels.DSDSetViewModels
{
    using Host;
    using System;

    public class DSDSetTr
    {
        public DSDSetTr() { }

        public static DSDSetTr Create(DSDSetState ssdSet)
        {
            return new DSDSetTr
            {
                DSDCard = ssdSet.DSDCard,
                CreateOn = ssdSet.CreateOn,
                Id = ssdSet.Id,
                IsEnabled = ssdSet.IsEnabled,
                Name = ssdSet.Name
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string Name { get; set; }

        public virtual int DSDCard { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int IsEnabled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime? CreateOn { get; set; }
    }
}
