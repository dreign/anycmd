﻿
namespace Anycmd.AC.ViewModels.RoleViewModels
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using ViewModel;

    public class GetPlistDSDSetRoles : GetPlistResult
    {
        /// <summary>
        /// 
        /// </summary>
        public string key { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public Guid dsdSetID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool? isAssigned { get; set; }
    }
}
