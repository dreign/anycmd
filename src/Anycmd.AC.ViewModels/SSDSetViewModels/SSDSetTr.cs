﻿
namespace Anycmd.AC.ViewModels.SSDSetViewModels
{
    using Host;
    using System;

    public class SSDSetTr
    {
        public SSDSetTr() { }

        public static SSDSetTr Create(SSDSetState ssdSet)
        {
            return new SSDSetTr
            {
                SSDCard = ssdSet.SSDCard,
                CreateOn = ssdSet.CreateOn,
                Id = ssdSet.Id,
                IsEnabled = ssdSet.IsEnabled,
                Name = ssdSet.Name
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string Name { get; set; }

        public virtual int SSDCard { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual int IsEnabled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime? CreateOn { get; set; }
    }
}
