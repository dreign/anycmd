﻿
namespace Anycmd.Mis.Web.Mvc.Controllers
{
    using Anycmd.Web.Mvc;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Web.Mvc;
    using Util;

    /// <summary>
    /// 首页控制器
    /// </summary>
    public class HomeController : AnycmdController
    {
        /// <summary>
        /// 首页
        /// </summary>
        [By("xuexs")]
        [CacheFilter]
        public ViewResultBase Index()
        {
            return View();
        }

        [By("xuexs")]
        public ViewResultBase About()
        {
            return ViewResult();
        }

        /// <summary>
        /// 登录
        /// </summary>
        [By("xuexs")]
        [CacheFilter]
        public ActionResult LogOn()
        {
            if (Host.UserSession.Principal.Identity.IsAuthenticated)
            {
                return this.RedirectToAction("Index");
            }
            return View();
        }

        private static ActionResult iconImgResult = null;
        [By("xuexs")]
        [IgnoreAuth]
        [Description("获取图标")]
        [CacheFilter(Enable = true)]
        public ActionResult GetIcons()
        {
            if (iconImgResult == null)
            {
                //[{'id':1,'phrase':'[呵呵]','url':'1.gif'},{'id':2,'phrase':'[嘻嘻]','url':'2.gif'}]
                var dtinfo = new DirectoryInfo(Server.MapPath("~/Content/icons/16x16/"));
                var files = dtinfo.GetFiles();
                var icons = files.Select(f => new
                {
                    id = f.Name.Substring(0, f.Name.Length - f.Extension.Length).ToLower(),
                    phrase = f.Name.Substring(0, f.Name.Length - f.Extension.Length),
                    icon = f.Name.ToLower(),
                    url = "Content/icons/16x16/" + f.Name,
                    extension = f.Extension
                });

                iconImgResult = this.JsonResult(icons);
            }

            return iconImgResult;
        }
    }
}