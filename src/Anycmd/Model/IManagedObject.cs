﻿
namespace Anycmd.Model
{
    using Host.EDI;
    using Host.Info;

    public interface IManagedObject
    {
        OntologyDescriptor Ontology { get; }
        InfoItem[] InputValues { get; }
        InfoItem[] Entity { get; }
    }
}
