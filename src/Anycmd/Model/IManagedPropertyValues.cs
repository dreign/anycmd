﻿
namespace Anycmd.Model
{
    using Host.EDI;
    using Host.Info;
    using System.Collections.Generic;

    public interface IManagedPropertyValues
    {
        IEnumerable<InfoItem> GetValues(OntologyDescriptor ontology);
    }
}
