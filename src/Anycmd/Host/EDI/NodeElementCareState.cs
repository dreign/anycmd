﻿
namespace Anycmd.Host.EDI
{
    using Anycmd.EDI;
    using Model;
    using System;

    public sealed class NodeElementCareState : StateObject<NodeElementCareState>, INodeElementCare, IStateObject
    {
        private NodeElementCareState() { }

        public static NodeElementCareState Create(INodeElementCare nodeElementCare)
        {
            if (nodeElementCare == null)
            {
                throw new ArgumentNullException("nodeElementCare");
            }
            return new NodeElementCareState
            {
                ElementID = nodeElementCare.ElementID,
                Id = nodeElementCare.Id,
                IsInfoIDItem = nodeElementCare.IsInfoIDItem,
                NodeID = nodeElementCare.NodeID
            };
        }

        public Guid ElementID { get; private set; }

        public Guid NodeID { get; private set; }

        public bool IsInfoIDItem { get; private set; }

        protected override bool DoEquals(NodeElementCareState other)
        {
            return ElementID == other.ElementID &&
                Id == other.Id &&
                IsInfoIDItem == other.IsInfoIDItem &&
                NodeID == other.NodeID;
        }
    }
}
