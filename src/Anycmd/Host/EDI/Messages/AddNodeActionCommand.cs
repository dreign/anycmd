﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class AddNodeActionCommand : AddEntityCommand<INodeActionCreateInput>, ISysCommand
    {
        public AddNodeActionCommand(INodeActionCreateInput input)
            : base(input)
        {

        }
    }
}
