﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class UpdateNodeActionCommand : UpdateEntityCommand<INodeActionUpdateInput>, ISysCommand
    {
        public UpdateNodeActionCommand(INodeActionUpdateInput input)
            : base(input)
        {

        }
    }
}
