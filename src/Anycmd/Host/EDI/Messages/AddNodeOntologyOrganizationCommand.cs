﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class AddNodeOntologyOrganizationCommand : AddEntityCommand<INodeOntologyOrganizationCreateInput>, ISysCommand
    {
        public AddNodeOntologyOrganizationCommand(INodeOntologyOrganizationCreateInput input)
            : base(input)
        {

        }
    }
}
