﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;
    using ValueObjects;

    public class NodeOntologyOrganizationAddedEvent: DomainEvent
    {
        #region Ctor
        public NodeOntologyOrganizationAddedEvent(NodeOntologyOrganizationBase source, INodeOntologyOrganizationCreateInput input)
            : base(source)
        {
            if (input == null)
            {
                throw new System.ArgumentNullException("input");
            }
            this.Input = input;
        }
        #endregion

        public INodeOntologyOrganizationCreateInput Input { get; private set; }
    }
}
