﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;

    public class NodeOntologyOrganizationRemovedEvent : DomainEvent {
        #region Ctor
        public NodeOntologyOrganizationRemovedEvent(NodeOntologyOrganizationBase source) : base(source) { }
        #endregion
    }
}
