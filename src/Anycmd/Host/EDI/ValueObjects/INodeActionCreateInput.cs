﻿using Anycmd.Model;

namespace Anycmd.Host.EDI.ValueObjects
{
    public interface INodeActionCreateInput : IEntityCreateInput
    {
    }
}
