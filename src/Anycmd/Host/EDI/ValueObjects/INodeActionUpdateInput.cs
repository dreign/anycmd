﻿using Anycmd.Model;

namespace Anycmd.Host.EDI.ValueObjects
{
    public interface INodeActionUpdateInput : IEntityUpdateInput
    {
    }
}
