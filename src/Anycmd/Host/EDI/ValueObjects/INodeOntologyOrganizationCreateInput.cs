﻿
namespace Anycmd.Host.EDI.ValueObjects
{
    using Model;
    using System;

    public interface INodeOntologyOrganizationCreateInput : IEntityCreateInput
    {
        Guid NodeID { get; }
        Guid OntologyID { get; }
        Guid OrganizationID { get; }
    }
}
