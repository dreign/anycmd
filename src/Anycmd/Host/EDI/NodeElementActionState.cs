﻿
namespace Anycmd.Host.EDI
{
    using Anycmd.EDI;
    using Model;
    using System;

    public sealed class NodeElementActionState : StateObject<NodeElementActionState>, INodeElementAction, IStateObject
    {
        private NodeElementActionState() { }

        public static NodeElementActionState Create(INodeElementAction nodeElementAction)
        {
            if (nodeElementAction == null)
            {
                throw new ArgumentNullException("nodeElementAction");
            }
            return new NodeElementActionState
            {
                ActionID = nodeElementAction.ActionID,
                CreateOn = nodeElementAction.CreateOn,
                ElementID = nodeElementAction.ElementID,
                Id = nodeElementAction.Id,
                IsAllowed = nodeElementAction.IsAllowed,
                IsAudit = nodeElementAction.IsAudit,
                NodeID = nodeElementAction.NodeID
            };
        }

        public Guid ActionID { get; private set; }

        public Guid ElementID { get; private set; }

        public bool IsAllowed { get; private set; }

        public bool IsAudit { get; private set; }

        public Guid NodeID { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(NodeElementActionState other)
        {
            return ActionID == other.ActionID && 
                CreateOn == other.CreateOn && 
                ElementID == other.ElementID && 
                Id == other.Id && 
                IsAllowed == other.IsAllowed &&
                IsAudit == other.IsAudit && 
                NodeID == other.NodeID;
        }
    }
}
