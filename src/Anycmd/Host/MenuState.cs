﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Exceptions;
    using Model;
    using System;

    public sealed class MenuState : StateObject<MenuState>, IMenu
    {
        private IAppHost AppHost { get; set; }
        private Guid _appSystemID;

        private MenuState() { }

        public static MenuState Create(IAppHost host, IMenu menu)
        {
            if (menu == null)
            {
                throw new ArgumentNullException("menu");
            }
            if (!host.AppSystemSet.ContainsAppSystem(menu.AppSystemID))
            {
                throw new ValidationException("意外的应用系统标识" + menu.AppSystemID);
            }
            return new MenuState
            {
                AppHost = host,
                Id = menu.Id,
                AppSystemID = menu.AppSystemID,
                Name = menu.Name,
                ParentID = menu.ParentID,
                Url = menu.Url,
                Icon = menu.Icon,
                SortCode = menu.SortCode,
                AllowDelete = menu.AllowDelete,
                AllowEdit = menu.AllowEdit,
                Description = menu.Description
            };
        }

        public Guid AppSystemID
        {
            get { return _appSystemID; }
            private set
            {
                if (!AppHost.AppSystemSet.ContainsAppSystem(value))
                {
                    throw new ValidationException("意外的功能应用系统标识" + value);
                }
                _appSystemID = value;
            }
        }

        public Guid? ParentID { get; private set; }

        public string Name { get; private set; }

        public string Url { get; private set; }

        public string Icon { get; private set; }

        public int SortCode { get; private set; }

        public int? AllowEdit { get; private set; }
        public int? AllowDelete { get; private set; }

        public string Description { get; private set; }

        protected override bool DoEquals(MenuState other)
        {
            return Id == other.Id &&
                AppSystemID == other.AppSystemID &&
                ParentID == other.ParentID &&
                Name == other.Name &&
                Url == other.Url &&
                Icon == other.Icon &&
                SortCode == other.SortCode &&
                AllowDelete == other.AllowDelete &&
                AllowEdit == other.AllowEdit &&
                Description == other.Description;
        }
    }
}
