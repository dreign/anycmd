﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Exceptions;
    using Model;
    using System;
    using Util;

    /// <summary>
    /// <see cref="IFunction"/>
    /// </summary>
    public sealed class FunctionState : StateObject<FunctionState>, IFunction, IStateObject
    {
        private Guid _resourceTypeID;

        public static readonly FunctionState Empty = new FunctionState
        {
            Code = string.Empty,
            CreateOn = SystemTime.MinDate,
            Description = string.Empty,
            DeveloperID = Guid.Empty,
            Id = Guid.Empty,
            _resourceTypeID = Guid.Empty,
            IsEnabled = 0,
            IsManaged = false,
            SortCode = 0
        };

        private FunctionState() { }

        public static FunctionState Create(IAppHost host, FunctionBase function)
        {
            if (function == null)
            {
                throw new ArgumentNullException("function");
            }
            if (function.ResourceTypeID == Guid.Empty)
            {
                throw new CoreException("必须指定资源");
            }
            ResourceTypeState resource;
            if (!host.ResourceTypeSet.TryGetResource(function.ResourceTypeID, out resource))
            {
                throw new ValidationException("非法的资源标识" + function.ResourceTypeID);
            }
            return new FunctionState
            {
                AppHost = host,
                Id = function.Id,
                ResourceTypeID = function.ResourceTypeID,
                Code = function.Code,
                IsManaged = function.IsManaged,
                IsEnabled = function.IsEnabled,
                DeveloperID = function.DeveloperID,
                SortCode = function.SortCode,
                Description = function.Description,
                CreateOn = function.CreateOn
            };
        }

        public IAppHost AppHost { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid ResourceTypeID
        {
            get { return _resourceTypeID; }
            private set
            {
                ResourceTypeState resource;
                if (!AppHost.ResourceTypeSet.TryGetResource(value, out resource))
                {
                    throw new ValidationException("意外的功能资源标识" + value);
                }
                _resourceTypeID = value;
            }
        }

        public string Code { get; private set; }

        public bool IsManaged { get; private set; }

        public int IsEnabled { get; private set; }

        public Guid DeveloperID { get; set; }

        public int SortCode { get; private set; }

        public string Description { get; private set; }

        public DateTime? CreateOn { get; private set; }

        public AppSystemState AppSystem
        {
            get
            {
                if (this == Empty)
                {
                    return AppSystemState.Empty;
                }
                AppSystemState appSystem;
                if (!AppHost.AppSystemSet.TryGetAppSystem(this.Resource.AppSystemID, out appSystem))
                {
                    throw new CoreException("意外的应用系统标识");
                }
                return appSystem;
            }
        }

        public ResourceTypeState Resource
        {
            get
            {
                if (this == Empty)
                {
                    return ResourceTypeState.Empty;
                }
                ResourceTypeState resource;
                if (!AppHost.ResourceTypeSet.TryGetResource(this.ResourceTypeID, out resource))
                {
                    throw new CoreException("意外的资源标识");
                }
                return resource;
            }
        }

        protected override bool DoEquals(FunctionState other)
        {
            return Id == other.Id &&
                ResourceTypeID == other.ResourceTypeID &&
                Code == other.Code &&
                IsManaged == other.IsManaged &&
                IsEnabled == other.IsEnabled &&
                DeveloperID == other.DeveloperID &&
                SortCode == other.SortCode &&
                Description == other.Description;
        }
    }
}
