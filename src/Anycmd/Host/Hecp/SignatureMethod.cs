﻿
namespace Anycmd.Host.Hecp
{

    /// <summary>
    /// 加密算法枚举
    /// </summary>
    public enum SignatureMethod : byte
    {
        /// <summary>
        /// 
        /// </summary>
        Undefined = 0,
        /// <summary>
        /// 
        /// </summary>
        HMAC_SHA1 = 1
    }
}
