﻿
namespace Anycmd.Host.Hecp
{

    public interface IHecpHandler
    {
        void Process(HecpContext context);
    }
}
