﻿
namespace Anycmd.Host
{
    using System;

    public interface IUserSessionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionID"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        IUserSession CreateSession(IAppHost host, Guid sessionID, AccountState account);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionID"></param>
        void DeleteSession(IAppHost host, Guid sessionID);
    }
}
