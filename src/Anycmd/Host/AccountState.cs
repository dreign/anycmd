﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Identity;
    using Model;
    using System;
    using Util;

    public sealed class AccountState : StateObject<AccountState>, IAccount, IStateObject
    {
        /// <summary>
        /// 空账户
        /// </summary>
        public static readonly AccountState Empty = new AccountState
        {
            NumberID = int.MinValue,
            CreateOn = SystemTime.MinDate,
            Id = Guid.Empty,
            LoginName = string.Empty,
            Code = string.Empty,
            Email = string.Empty,
            Mobile = string.Empty,
            Name = string.Empty,
            QQ = string.Empty
        };

        private AccountState() { }

        public static AccountState Create(AccountBase account)
        {
            if (account == null)
            {
                return Empty;
            }
            return new AccountState()
            {
                NumberID = account.NumberID,
                Id = account.Id,
                LoginName = account.LoginName,
                CreateOn = account.CreateOn,
                Code = account.Code,
                Email = account.Email,
                Mobile = account.Mobile,
                Name = account.Name,
                QQ = account.QQ
            };
        }

        public int NumberID { get; private set; }

        public string LoginName { get; private set; }

        public DateTime? CreateOn { get; private set; }

        public string Name { get; private set; }

        public string Code { get; private set; }

        public string Email { get; private set; }

        public string QQ { get; private set; }

        public string Mobile { get; private set; }

        protected override bool DoEquals(AccountState other)
        {
            return Id == other.Id &&
                LoginName == other.LoginName &&
                NumberID == other.NumberID &&
                Name == other.Name &&
                Code == other.Code &&
                Email == other.Email &&
                QQ == other.QQ &&
                Mobile == other.Mobile;
        }
    }
}
