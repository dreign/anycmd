﻿
namespace Anycmd.Host.AC
{
    using Identity.Messages;
    using Infra.Messages;
    using Messages;
    using System;
    using ValueObjects;

    public static class AppHostExtension
    {
        public static void AddAppSystem(this IAppHost host, IAppSystemCreateInput input)
        {
            host.Handle(new AddAppSystemCommand(input));
        }

        public static void UpdateAppSystem(this IAppHost host, IAppSystemUpdateInput input)
        {
            host.Handle(new UpdateAppSystemCommand(input));
        }

        public static void RemoveAppSystem(this IAppHost host, Guid appSystemID)
        {
            host.Handle(new RemoveAppSystemCommand(appSystemID));
        }

        public static void AssignPassword(this IAppHost host, IPasswordAssignInput input)
        {
            host.Handle(new AssignPasswordCommand(input));
        }

        public static void ChangePassword(this IAppHost host, IPasswordChangeInput input)
        {
            host.Handle(new ChangePasswordCommand(input));
        }

        public static void AddAccount(this IAppHost host, IAccountCreateInput input)
        {
            host.Handle(new AddAccountCommand(input));
        }

        public static void UpdateAccount(this IAppHost host, IAccountUpdateInput input)
        {
            host.Handle(new UpdateAccountCommand(input));
        }

        public static void RemoveAccount(this IAppHost host, Guid accountID)
        {
            host.Handle(new RemoveAccountCommand(accountID));
        }

        public static void RemovePrivilegeBigram(this IAppHost host, Guid privilegeBigramID)
        {
            host.Handle(new RemovePrivilegeBigramCommand(privilegeBigramID));
        }

        public static void UpdatePrivilegeBigram(this IAppHost host, IPrivilegeBigramUpdateInput input)
        {
            host.Handle(new UpdatePrivilegeBigramCommand(input));
        }

        public static void AddPrivilegeBigram(this IAppHost host, IPrivilegeBigramCreateInput input)
        {
            host.Handle(new AddPrivilegeBigramCommand(input));
        }

        public static void AddButton(this IAppHost host, IButtonCreateInput input)
        {
            host.Handle(new AddButtonCommand(input));
        }

        public static void UpdateButton(this IAppHost host, IButtonUpdateInput input)
        {
            host.Handle(new UpdateButtonCommand(input));
        }

        public static void RemoveButton(this IAppHost host, Guid buttonID)
        {
            host.Handle(new RemoveButtonCommand(buttonID));
        }

        public static void AddDic(this IAppHost host, IDicCreateInput input)
        {
            host.Handle(new AddDicCommand(input));
        }

        public static void UpdateDic(this IAppHost host, IDicUpdateInput input)
        {
            host.Handle(new UpdateDicCommand(input));
        }

        public static void RemoveDic(this IAppHost host, Guid dicID)
        {
            host.Handle(new RemoveDicCommand(dicID));
        }

        public static void AddDicItem(this IAppHost host, IDicItemCreateInput input)
        {
            host.Handle(new AddDicItemCommand(input));
        }

        public static void UpdateDicItem(this IAppHost host, IDicItemUpdateInput input)
        {
            host.Handle(new UpdateDicItemCommand(input));
        }

        public static void RemoveDicItem(this IAppHost host, Guid dicItemID)
        {
            host.Handle(new RemoveDicItemCommand(dicItemID));
        }

        public static void AddEntityType(this IAppHost host, IEntityTypeCreateInput input)
        {
            host.Handle(new AddEntityTypeCommand(input));
        }

        public static void UpdateEntityType(this IAppHost host, IEntityTypeUpdateInput input)
        {
            host.Handle(new UpdateEntityTypeCommand(input));
        }

        public static void RemoveEntityType(this IAppHost host, Guid entityTypeID)
        {
            host.Handle(new RemoveEntityTypeCommand(entityTypeID));
        }

        public static void AddFunction(this IAppHost host, IFunctionCreateInput input)
        {
            host.Handle(new AddFunctionCommand(input));
        }
    }
}
