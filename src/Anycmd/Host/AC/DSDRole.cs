﻿
namespace Anycmd.Host.AC
{
    using Anycmd.AC;
    using Model;
    using ValueObjects;

    public class DSDRole : DSDRoleBase, IAggregateRoot
    {
        public static DSDRole Create(IDSDRoleCreateInput input)
        {
            return new DSDRole
            {
                Id = input.Id.Value,
                RoleID = input.RoleID,
                DSDSetID = input.DSDSetID
            };
        }
    }
}
