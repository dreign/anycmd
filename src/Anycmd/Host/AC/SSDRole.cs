﻿
namespace Anycmd.Host.AC
{
    using Anycmd.AC;
    using Model;
    using ValueObjects;


    public class SSDRole : SSDRoleBase, IAggregateRoot
    {
        public static SSDRole Create(ISSDRoleCreateInput input)
        {
            return new SSDRole
            {
                Id = input.Id.Value,
                SSDSetID = input.SSDSetID,
                RoleID = input.RoleID
            };
        }
    }
}
