﻿
namespace Anycmd.Host.AC.Infra
{
    using Anycmd.AC.Infra;
    using Model;
    using ValueObjects;

    /// <summary>
    /// 系统界面视图。设计用于支持界面视图级配置和自动节点等。
    /// </summary>
    public class UIView : UIViewBase, IAggregateRoot
    {
        public UIView() { }

        public static UIView Create(IUIViewCreateInput input)
        {
            return new UIView
            {
                Id = input.Id.Value,
                Icon = input.Icon,
                Tooltip = input.Tooltip
            };
        }

        public void Update(IUIViewUpdateInput input)
        {
            this.Tooltip = input.Tooltip;
            this.Icon = input.Icon;
        }
    }
}
