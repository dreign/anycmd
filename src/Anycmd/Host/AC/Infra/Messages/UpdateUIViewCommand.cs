﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class UpdateUIViewCommand : UpdateEntityCommand<IUIViewUpdateInput>, ISysCommand
    {
        public UpdateUIViewCommand(IUIViewUpdateInput input)
            : base(input)
        {

        }
    }
}
