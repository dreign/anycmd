﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using ValueObjects;


    public class AddUIViewCommand : AddEntityCommand<IUIViewCreateInput>, ISysCommand
    {
        public AddUIViewCommand(IUIViewCreateInput input)
            : base(input)
        {

        }
    }
}
