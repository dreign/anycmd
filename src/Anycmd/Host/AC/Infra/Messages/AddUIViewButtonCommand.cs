﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using ValueObjects;


    public class AddUIViewButtonCommand : AddEntityCommand<IUIViewButtonCreateInput>, ISysCommand
    {
        public AddUIViewButtonCommand(IUIViewButtonCreateInput input)
            : base(input)
        {

        }
    }
}
