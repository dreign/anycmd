﻿using Anycmd.AC.Infra;
using Anycmd.Events;

namespace Anycmd.Host.AC.Infra.Messages
{
    public class UIViewRemovingEvent: DomainEvent
    {
        #region Ctor
        public UIViewRemovingEvent(UIViewBase source)
            : base(source)
        {
        }
        #endregion
    }
}
