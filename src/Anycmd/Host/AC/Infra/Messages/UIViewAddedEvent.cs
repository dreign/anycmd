﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using ValueObjects;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewAddedEvent : EntityAddedEvent<IUIViewCreateInput>
    {
        #region Ctor
        public UIViewAddedEvent(UIViewBase source, IUIViewCreateInput input)
            : base(source, input)
        {
        }
        #endregion
    }
}
