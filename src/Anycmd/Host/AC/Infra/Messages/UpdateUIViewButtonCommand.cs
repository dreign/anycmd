﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using ValueObjects;


    public class UpdateUIViewButtonCommand : UpdateEntityCommand<IUIViewButtonUpdateInput>, ISysCommand
    {
        public UpdateUIViewButtonCommand(IUIViewButtonUpdateInput input)
            : base(input)
        {

        }
    }
}
