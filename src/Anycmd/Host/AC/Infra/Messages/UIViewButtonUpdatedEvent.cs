﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using AC.ValueObjects;
    using Anycmd.AC.Infra;
    using Anycmd.Events;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewButtonUpdatedEvent : DomainEvent
    {
        #region Ctor
        public UIViewButtonUpdatedEvent(UIViewButtonBase source, IUIViewButtonUpdateInput input)
            : base(source)
        {
            if (input == null)
            {
                throw new System.ArgumentNullException("input");
            }
            this.Input = input;
        }
        #endregion

        public IUIViewButtonUpdateInput Input { get; private set; }
    }
}
