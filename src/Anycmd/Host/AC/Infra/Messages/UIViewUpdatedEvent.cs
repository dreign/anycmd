﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using AC.ValueObjects;
    using Anycmd.AC.Infra;
    using Anycmd.Events;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewUpdatedEvent : DomainEvent
    {
        #region Ctor
        public UIViewUpdatedEvent(UIViewBase source, IUIViewUpdateInput input)
            : base(source)
        {
            if (input == null)
            {
                throw new System.ArgumentNullException("input");
            }
            this.Input = input;
        }
        #endregion

        public IUIViewUpdateInput Input { get; private set; }
    }
}
