﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using System;

    public class RemoveUIViewButtonCommand : RemoveEntityCommand, ISysCommand
    {
        public RemoveUIViewButtonCommand(Guid viewButtonID)
            : base(viewButtonID)
        {

        }
    }
}
