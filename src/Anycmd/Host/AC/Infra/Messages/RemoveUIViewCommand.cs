﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using System;

    public class RemoveUIViewCommand : RemoveEntityCommand, ISysCommand
    {
        public RemoveUIViewCommand(Guid viewID)
            : base(viewID)
        {

        }
    }
}
