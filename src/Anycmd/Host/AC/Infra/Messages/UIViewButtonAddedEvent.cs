﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using ValueObjects;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewButtonAddedEvent : EntityAddedEvent<IUIViewButtonCreateInput>
    {
        #region Ctor
        public UIViewButtonAddedEvent(UIViewButtonBase source, IUIViewButtonCreateInput input)
            : base(source, input)
        {
        }
        #endregion
    }
}
