﻿
namespace Anycmd.Host.AC
{
    using Anycmd.AC;
    using Model;
    using ValueObjects;

    public class DSDSet : DSDSetBase, IAggregateRoot
    {
        public static DSDSet Create(IDSDSetCreateInput input)
        {
            return new DSDSet
            {
                Id = input.Id.Value,
                Description = input.Description,
                IsEnabled = input.IsEnabled,
                Name = input.Name,
                DSDCard = input.DSDCard
            };
        }

        public void Update(IDSDSetUpdateInput input)
        {
            this.Description = input.Description;
            this.IsEnabled = input.IsEnabled;
            this.Name = input.Name;
            this.DSDCard = input.DSDCard;
        }
    }
}
