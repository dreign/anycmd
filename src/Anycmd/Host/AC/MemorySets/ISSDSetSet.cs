﻿
namespace Anycmd.Host.AC.MemorySets
{
    using System;
    using System.Collections.Generic;

    public interface ISSDSetSet : IEnumerable<SSDSetState>
    {
        bool TryGetSSDSet(Guid ssdSetID, out SSDSetState ssdSet);

        IReadOnlyCollection<SSDRoleState> GetSSDRoles(SSDSetState ssdSet);

        IReadOnlyCollection<SSDRoleState> GetSSDRoles();

        bool CheckRoles(IEnumerable<RoleState> roles, out string msg);
    }
}
