﻿
namespace Anycmd.Host.AC.MemorySets
{
    using System;
    using System.Collections.Generic;

    public interface IDSDSetSet : IEnumerable<DSDSetState>
    {
        bool TryGetDSDSet(Guid dsdSetID, out DSDSetState dsdSet);

        IReadOnlyCollection<DSDRoleState> GetDSDRoles(DSDSetState dsdSet);

        IReadOnlyCollection<DSDRoleState> GetDSDRoles();

        bool CheckRoles(IEnumerable<RoleState> roles, out string msg);
    }
}
