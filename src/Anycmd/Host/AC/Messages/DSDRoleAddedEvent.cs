﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using ValueObjects;

    public class DSDRoleAddedEvent : EntityAddedEvent<IDSDRoleCreateInput>
    {
        #region Ctor
        public DSDRoleAddedEvent(DSDRoleBase source, IDSDRoleCreateInput input)
            : base(source, input)
        {
        }
        #endregion
    }
}
