﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using ValueObjects;

    public class DSDSetAddedEvent : EntityAddedEvent<IDSDSetCreateInput>
    {
        #region Ctor
        public DSDSetAddedEvent(DSDSetBase source, IDSDSetCreateInput input)
            : base(source, input)
        {
        }
        #endregion
    }
}
