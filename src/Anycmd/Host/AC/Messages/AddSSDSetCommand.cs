﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class AddSSDSetCommand : AddEntityCommand<ISSDSetCreateInput>, ISysCommand
    {
        public AddSSDSetCommand(ISSDSetCreateInput input)
            : base(input)
        {

        }
    }
}
