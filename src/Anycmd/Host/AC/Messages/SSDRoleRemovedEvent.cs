﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;

    public class SSDRoleRemovedEvent : DomainEvent
    {
        #region Ctor
        public SSDRoleRemovedEvent(SSDRoleBase source)
            : base(source)
        {
        }
        #endregion
    }
}