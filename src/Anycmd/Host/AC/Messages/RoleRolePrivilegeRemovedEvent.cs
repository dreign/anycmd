﻿using Anycmd.AC;
using Anycmd.Events;

namespace Anycmd.Host.AC.Messages
{
    public class RoleRolePrivilegeRemovedEvent : DomainEvent
    {
        #region Ctor
        public RoleRolePrivilegeRemovedEvent(PrivilegeBigramBase source)
            : base(source)
        {
        }
        #endregion
    }
}
