﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class AddDSDSetCommand : AddEntityCommand<IDSDSetCreateInput>, ISysCommand
    {
        public AddDSDSetCommand(IDSDSetCreateInput input)
            : base(input)
        {

        }
    }
}
