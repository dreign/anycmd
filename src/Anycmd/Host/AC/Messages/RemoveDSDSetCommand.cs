﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using System;

    public class RemoveDSDSetCommand : RemoveEntityCommand, ISysCommand
    {
        public RemoveDSDSetCommand(Guid dsdSetID)
            : base(dsdSetID)
        {

        }
    }
}
