﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class UpdatePositionCommand : UpdateEntityCommand<IPositionUpdateInput>, ISysCommand
    {
        public UpdatePositionCommand(IPositionUpdateInput input)
            : base(input)
        {

        }
    }
}
