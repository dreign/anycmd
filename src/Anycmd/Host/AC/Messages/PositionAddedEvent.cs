﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using ValueObjects;

    public class PositionAddedEvent : EntityAddedEvent<IPositionCreateInput>
    {
        #region Ctor
        public PositionAddedEvent(GroupBase source, IPositionCreateInput input)
            : base(source, input)
        {
        }
        #endregion
    }
}
