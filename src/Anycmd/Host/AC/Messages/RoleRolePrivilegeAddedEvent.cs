﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;

    public class RoleRolePrivilegeAddedEvent : DomainEvent
    {
        #region Ctor
        public RoleRolePrivilegeAddedEvent(PrivilegeBigramBase source)
            : base(source)
        {
        }
        #endregion
    }
}
