﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using System;

    public class RemoveSSDSetCommand: RemoveEntityCommand, ISysCommand
    {
        public RemoveSSDSetCommand(Guid ssdSetID)
            : base(ssdSetID)
        {

        }
    }
}
