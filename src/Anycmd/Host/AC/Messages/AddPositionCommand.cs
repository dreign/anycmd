﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class AddPositionCommand : AddEntityCommand<IPositionCreateInput>, ISysCommand
    {
        public AddPositionCommand(IPositionCreateInput input)
            : base(input)
        {
        }
    }
}
