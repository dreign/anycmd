﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class UpdateDSDSetCommand : UpdateEntityCommand<IDSDSetUpdateInput>, ISysCommand
    {
        public UpdateDSDSetCommand(IDSDSetUpdateInput input)
            : base(input)
        {

        }
    }
}
