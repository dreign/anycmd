﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;
    using ValueObjects;

    public class SSDSetUpdatedEvent: DomainEvent
    {
        #region Ctor
        public SSDSetUpdatedEvent(SSDSetBase source, ISSDSetUpdateInput input)
            : base(source)
        {
            if (input == null)
            {
                throw new System.ArgumentNullException("input");
            }
            this.Input = input;
        }
        #endregion

        public ISSDSetUpdateInput Input { get; private set; }
    }
}