﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class AddSSDRoleCommand : AddEntityCommand<ISSDRoleCreateInput>, ISysCommand
    {
        public AddSSDRoleCommand(ISSDRoleCreateInput input)
            : base(input)
        {

        }
    }
}
