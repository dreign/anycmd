﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using ValueObjects;

    public class AddDSDRoleCommand : AddEntityCommand<IDSDRoleCreateInput>, ISysCommand
    {
        public AddDSDRoleCommand(IDSDRoleCreateInput input)
            : base(input)
        {

        }
    }
}
