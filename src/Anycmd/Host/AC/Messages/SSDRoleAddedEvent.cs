﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using ValueObjects;

    public class SSDRoleAddedEvent : EntityAddedEvent<ISSDRoleCreateInput>
    {
        #region Ctor
        public SSDRoleAddedEvent(SSDRoleBase source, ISSDRoleCreateInput input)
            : base(source, input)
        {
        }
        #endregion
    }
}
