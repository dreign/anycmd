﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;

    public class DSDSetRemovedEvent : DomainEvent
    {
        #region Ctor
        public DSDSetRemovedEvent(DSDSetBase source)
            : base(source)
        {
        }
        #endregion
    }
}