﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using System;

    public class RemoveDSDRoleCommand : RemoveEntityCommand, ISysCommand
    {
        public RemoveDSDRoleCommand(Guid dsdRoleID)
            : base(dsdRoleID)
        {

        }
    }
}
