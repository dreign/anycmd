﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;
    using ValueObjects;

    public class DSDSetUpdatedEvent : DomainEvent
    {
        #region Ctor
        public DSDSetUpdatedEvent(DSDSetBase source, IDSDSetUpdateInput input)
            : base(source)
        {
            if (input == null)
            {
                throw new System.ArgumentNullException("input");
            }
            this.Input = input;
        }
        #endregion

        public IDSDSetUpdateInput Input { get; private set; }
    }
}