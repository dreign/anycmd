﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using ValueObjects;

    public class SSDSetAddedEvent: EntityAddedEvent<ISSDSetCreateInput>
    {
        #region Ctor
        public SSDSetAddedEvent(SSDSetBase source, ISSDSetCreateInput input)
            : base(source, input)
        {
        }
        #endregion
    }
}
