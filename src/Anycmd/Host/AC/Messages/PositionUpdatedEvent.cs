﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;
    using ValueObjects;

    public class PositionUpdatedEvent : DomainEvent
    {
        #region Ctor
        public PositionUpdatedEvent(GroupBase source, IPositionUpdateInput input)
            : base(source)
        {
            if (input == null)
            {
                throw new System.ArgumentNullException("input");
            }
            this.Input = input;
        }
        #endregion

        public IPositionUpdateInput Input { get; private set; }
    }
}