﻿using System;

namespace Anycmd.Host.AC.ValueObjects
{
    using Model;

    public interface IUIViewUpdateInput : IEntityUpdateInput
    {
        string Icon { get; }
        string Tooltip { get; }
    }
}
