﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;

    public class SSDSetCreateInput : EntityCreateInput, IInputModel, ISSDSetCreateInput
    {
        public string Name { get; set; }

        public int SSDCard { get; set; }

        public int IsEnabled { get; set; }

        public string Description { get; set; }
    }
}
