﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;
    using System;

    public class SSDRoleCreateInput : EntityCreateInput, IInputModel, ISSDRoleCreateInput
    {
        public Guid SSDSetID { get; set; }

        public Guid RoleID { get; set; }
    }
}
