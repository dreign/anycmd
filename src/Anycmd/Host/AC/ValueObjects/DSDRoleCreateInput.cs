﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;
    using System;

    public class DSDRoleCreateInput : EntityCreateInput, IInputModel, IDSDRoleCreateInput
    {
        public Guid DSDSetID { get; set; }

        public Guid RoleID { get; set; }
    }
}
