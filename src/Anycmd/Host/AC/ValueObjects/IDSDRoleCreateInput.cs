﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;
    using System;

    public interface IDSDRoleCreateInput : IEntityCreateInput
    {
        Guid DSDSetID { get; }
        Guid RoleID { get; }
    }
}
