﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;

    public class DSDSetCreateInput : EntityCreateInput, IInputModel, IDSDSetCreateInput
    {
        public string Name { get; set; }

        public int DSDCard { get; set; }

        public int IsEnabled { get; set; }

        public string Description { get; set; }
    }
}
