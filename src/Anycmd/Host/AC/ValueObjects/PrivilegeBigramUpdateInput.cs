﻿using System;

namespace Anycmd.Host.AC.ValueObjects
{
    using Model;

    public class PrivilegeBigramUpdateInput : IInputModel, IPrivilegeBigramUpdateInput
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PrivilegeConstraint { get; set; }
    }
}
