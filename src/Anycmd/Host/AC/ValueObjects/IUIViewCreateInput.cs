﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;

    public interface IUIViewCreateInput : IEntityCreateInput
    {
        string Icon { get; }
        string Tooltip { get; }
    }
}
