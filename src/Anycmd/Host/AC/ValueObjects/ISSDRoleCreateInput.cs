﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;
    using System;

    public interface ISSDRoleCreateInput : IEntityCreateInput
    {
        Guid SSDSetID { get; }
        Guid RoleID { get; }
    }
}
