﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;

    public interface IPositionUpdateInput : IEntityUpdateInput
    {
        string CategoryCode { get; }
        string Description { get; }
        int IsEnabled { get; }
        string Name { get; }
        string ShortName { get; }
        int SortCode { get; }
    }
}
