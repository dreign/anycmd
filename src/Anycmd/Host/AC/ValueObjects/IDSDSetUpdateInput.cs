﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;

    public interface IDSDSetUpdateInput : IEntityUpdateInput
    {
        string Name { get; }

        /// <summary>
        /// 阀值
        /// </summary>
        int DSDCard { get; }

        /// <summary>
        /// 是否启用
        /// </summary>
        int IsEnabled { get; }

        string Description { get; }
    }
}
