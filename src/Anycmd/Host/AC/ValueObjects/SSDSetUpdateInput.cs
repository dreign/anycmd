﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;
    using System;

    public class SSDSetUpdateInput : IInputModel, ISSDSetUpdateInput
    {
        public string Name { get; set; }

        public int SSDCard { get; set; }

        public int IsEnabled { get; set; }

        public string Description { get; set; }

        public Guid Id { get; set; }
    }
}
