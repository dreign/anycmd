﻿
namespace Anycmd.Host.AC.ValueObjects
{
    using Model;
    using System;

    public class DSDSetUpdateInput : IInputModel, IDSDSetUpdateInput
    {
        public string Name { get; set; }

        public int DSDCard { get; set; }

        public int IsEnabled { get; set; }

        public string Description { get; set; }

        public Guid Id { get; set; }
    }
}
