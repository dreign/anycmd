﻿using System;

namespace Anycmd.Host.AC.ValueObjects
{
    using Model;

    public interface IUIViewButtonUpdateInput : IEntityUpdateInput
    {
        Guid? FunctionID { get; }
        int IsEnabled { get; }
    }
}
