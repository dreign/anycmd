﻿
namespace Anycmd.Host.AC
{
    using Anycmd.AC;
    using Model;
    using ValueObjects;

    public class SSDSet : SSDSetBase, IAggregateRoot
    {
        public static SSDSet Create(ISSDSetCreateInput input)
        {
            return new SSDSet
            {
                Id = input.Id.Value,
                Description = input.Description,
                IsEnabled = input.IsEnabled,
                Name = input.Name,
                SSDCard = input.SSDCard
            };
        }

        public void Update(ISSDSetUpdateInput input)
        {
            this.Description = input.Description;
            this.IsEnabled = input.IsEnabled;
            this.Name = input.Name;
            this.SSDCard = input.SSDCard;
        }
    }
}
