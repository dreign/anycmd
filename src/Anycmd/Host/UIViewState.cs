﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Model;
    using System;
    using Util;

    public sealed class UIViewState : StateObject<UIViewState>, IUIView, IStateObject
    {
        public static readonly UIViewState Empty = new UIViewState
        {
            CreateOn = SystemTime.MinDate,
            Icon = string.Empty,
            Id = Guid.Empty,
            Tooltip = string.Empty
        };

        private UIViewState() { }

        public static UIViewState Create(IAppHost host, UIViewBase view)
        {
            if (view == null)
            {
                throw new ArgumentNullException("view");
            }

            return new UIViewState
            {
                AppHost = host,
                Id = view.Id,
                Tooltip = view.Tooltip,
                CreateOn = view.CreateOn,
                Icon = view.Icon,
            };
        }

        public IAppHost AppHost { get; private set; }

        public string Tooltip { get; set; }

        public string Icon { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(UIViewState other)
        {
            return Id == other.Id &&
                Tooltip == other.Tooltip &&
                Icon == other.Icon;
        }
    }
}
