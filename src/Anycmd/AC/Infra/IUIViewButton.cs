﻿
namespace Anycmd.AC.Infra
{
    using System;

    /// <summary>
    /// 界面视图菜单模型接口
    /// </summary>
    public interface IUIViewButton
    {
        /// <summary>
        /// 
        /// </summary>
        Guid Id { get; }
        /// <summary>
        /// 
        /// </summary>
        Guid UIViewID { get; }
        /// <summary>
        /// 
        /// </summary>
        Guid? FunctionID { get; }
        /// <summary>
        /// 
        /// </summary>
        Guid ButtonID { get; }

        /// <summary>
        /// 菜单在界面的有效状态
        /// <remarks>是否可点击的意思</remarks>
        /// </summary>
        int IsEnabled { get; }
    }
}
