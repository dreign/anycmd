﻿
namespace Anycmd.AC.Infra
{
    using Exceptions;
    using Model;
    using System;

    public abstract class FunctionIOBase : EntityBase, IFunctionIO
    {
        private string _code;
        private string _name;

        public Guid FunctionID { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual string Code
        {
            get { return _code; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ValidationException("编码是必须的");
                }
                value = value.Trim();
                if (value != _code)
                {
                    _code = value;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public virtual string Name
        {
            get { return _name; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ValidationException("名称是必须的");
                }
                if (value != _name)
                {
                    _name = value;
                }
            }
        }

        public string Direction { get; set; }

        public string Description { get; set; }
    }
}
