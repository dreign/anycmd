﻿
namespace Anycmd.AC.Infra
{
    using System;

    public interface IFunctionIO
    {
        /// <summary>
        /// 按钮标识
        /// </summary>
        Guid Id { get; }
        /// <summary>
        /// 
        /// </summary>
        Guid FunctionID { get; }
        /// <summary>
        /// 
        /// </summary>
        string Direction { get; }
        /// <summary>
        /// 编码
        /// </summary>
        string Code { get; }
        /// <summary>
        /// 名称
        /// </summary>
        string Name { get; }
    }
}
