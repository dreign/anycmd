﻿using System;

namespace Anycmd.AC
{
    public interface ISSDRole
    {
        Guid Id { get; }
        Guid SSDSetID { get; }
        Guid RoleID { get; }
    }
}
