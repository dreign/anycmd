﻿
namespace Anycmd.AC
{
    using System;

    /// <summary>
    /// 动态责任分离角色集
    /// </summary>
    public interface IDSDSet
    {
        Guid Id { get; }
        string Name { get; }
        /// <summary>
        /// 是否启用
        /// </summary>
        int IsEnabled { get; }
        int DSDCard { get; }
        string Description { get; }
    }
}
