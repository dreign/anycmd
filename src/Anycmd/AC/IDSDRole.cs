﻿using System;

namespace Anycmd.AC
{
    public interface IDSDRole
    {
        Guid Id { get; }
        Guid DSDSetID { get; }
        Guid RoleID { get; }
    }
}
