﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class Schema2
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class VariableDefinitionElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string VariableDefinition = "VariableDefinition";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string VariableId = "VariableId";
            }
        }
    }
}