﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class Schema1
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class FunctionElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Function = "Function";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string FunctionId = "FunctionId";
            }
        }
    }
}