﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class Schema2
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class Namespaces
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Policy = "urn:oasis:names:tc:xacml:2.0:policy:schema:os";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Context = "urn:oasis:names:tc:xacml:2.0:context:schema:os";
            }
        }
    }
}