﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class Schema1
        {
            public static class MatchElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string MatchId = "MatchId";
            }
        }
    }
}