﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class Schema1
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class SubjectAttributeDesignatorElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string SubjectAttributeDesignator = "SubjectAttributeDesignator";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string SubjectCategory = "SubjectCategory";
            }
        }
    }
}