﻿
namespace Anycmd.Xacml
{
    /// <summary>
    /// Enumeration describing the document's type
    /// </summary>
    public enum DocumentType
    {
        /// <summary>
        /// Policy document
        /// </summary>
        Policy,

        /// <summary>
        /// Request document
        /// </summary>
        Request
    }
}
