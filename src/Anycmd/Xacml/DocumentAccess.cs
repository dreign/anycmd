
namespace Anycmd.Xacml
{
	/// <summary>
	/// Enumeration describing the access to the document
	/// </summary>
	public enum DocumentAccess
	{
		/// <summary>
		/// Read-only access
		/// </summary>
		ReadOnly,

		/// <summary>
		/// Read-write access
		/// </summary>
		ReadWrite
	}
}
