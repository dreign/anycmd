﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class ContextSchema
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class ResourceScope
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string ResourceScopeAttributeId = "urn:oasis:names:tc:xacml:1.0:resource:scope";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Immediate = "Immediate";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Children = "Children";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Descendants = "Descendants";
            }
        }
    }
}