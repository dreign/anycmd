﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class ContextSchema
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class SubjectElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string SubjectCategory = "SubjectCategory";
            }
        }
    }
}