﻿using Jint;
using System;
using Xunit;

namespace Anycmd.Jint.Tests
{
    public class HelloTest
    {
        [Fact]
        public void HelloJintTest()
        {
            var engine = new Engine()
                .SetValue("hello", (Func<string, string>)delegate(string a)
            {
                return a;
            });
            engine.Execute(@"
      function getWord(word) { 
        return hello(word);
      };

      getWord('Hello World');");
            var value = engine.GetCompletionValue().ToObject();
            Assert.Equal("Hello World", value);
            engine.Execute("getWord('hello anycmd')");
            value = engine.GetCompletionValue().ToObject();
            Assert.Equal("hello anycmd", value);
        }
    }
}
